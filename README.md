# Picking Station #

### Beschreibung ###
Im Logistiklabor der Technischen Hochschule Ulm wird ein stationäre Kommissionierstation mit einem UR5e aufgebaut. Dieser können Kommissionieraufträge für vorher trainierte Objekte übergeben werden, die automatisiert bearbeitet werden können. Die Position der Objekte im Regal ist dabei irrelevant und wird zur Laufzeit von der PickingStation ermittelt. Der Demonstrator soll außerdem die Abwicklung eines Emergency Call aufzeigen, d.h. die Unterstützung durch einen Menschen bei nicht erfolgreicher Objekterkennung.

### Komponenten (Hardware) ###
Ein UR5e wird stationär vor einem Kommissionier-Regal installiert und ein Kamerasystem von Roboception ist auf einer fixen Position hinter dem Roboter angebracht. Alle Komponenten sind via Ethernet über ein Switch miteinander verbunden.

### Systemanforderungen ###
- Der Demonstrator verwendet Jadex Version 3.0.115. Eine damit kompatible Java-Version ist 1.8.0_261.
- Als Umgebung zur Objekterkennung wird eine Windows-Installation von Darknet/Yolo (v3) verwendet.
- Für die Darstellung von Bildern in der GUI des Emergency Call wird 'thumbnailator' benutzt.
- Der Zugriff auf die REST-API der Robotception-Kamera erfolgt über deren Java-Client (https://github.com/roboception/rcapi_java).

### Multiagenten-System ###
Die Komponenten der PickingStation werden als Multiagenten-System implementiert. Ein Agent ist für die Einsteuerung von Aufträgen über eine GUI zuständig, die von der Picking Station automatisch bearbeitet werden. Diese Bearbeitung beinhaltet die operative Objekterkennung und den physischen Greifvorgang aus einem Regal. Ein Emergency Call bei nicht erfolgreicher Objekterkennung wird über eine GUI an einem Laptop abgehandelt. Das Multiagenten-System mit dem Ablauf einer Kommissionierung wird in der Dokumentation (Downloads -> Dokumentation.pdf) ausführlich beschrieben.

#### PickingStation-Agent ####
Dieser zentrale Agent ist für Bearbeitung der Kommissionieraufträge verantwortlich. Er stellt eine Benutzeroberfläche bereit, über die Aufträge angelegt und deren Bearbeitung überwacht werden kann. Durch ihn werden alle anderen Agenten getriggert, um einen einen Auftrag zu bearbeiten.

#### Roboception-Agent ####
Der Roboception Agent kann über die Software gc_stream.exe Bilddaten über die GenICam-Schnittstelle der Roboception-Kamera abrufen und über die REST-API eine Greifpunkt-Erkennung anstoßen.

#### PredictionServer-Agent ####
Dieser Agent startet die Objekterkennungin Darknet/Yolo mit einem vortrainierten Neuronalen Netz.

#### UR5e-Agent ####
Der Agent verbindet sich via Socket mit dem UR5e und kann Befehle senden sowie Rückmeldungen empfangen.

#### Photoneo-Agent (Entwicklung) ####
Die Technische Hochschule Ulm verfügt über ein Kamera-System von Photoneo. Dieses soll zukünftig auch für die PickingStation verwendet werden.

### Software (UR5e) ###
Ein UR-Skript wird auf dem UR5e implementiert, das sich mit einem vorgegebenen Socket-Server verbindet und auf Koordinaten wartet. Werden diese bereitgestellt, bewegt sich der Roboter zu dieser Position, greift das Objekt und setzt es an einer vorgegebenen Position ab. Danach gibt der UR5e eine Rückmeldung und wartet wieder auf Koordinaten.

### Acknowledgment ###
This work is part of the project “ZAFH Intralogistik”, funded by the European Regional Development Fund and the Ministry of Science, Research and Arts of Baden-Württemberg, Germany (F.No. 32-7545.24-17/3/1). It is also done within the post graduate school “Cognitive Computing in Socio-Technical Systems“ of Ulm University of Applied Sciences and Ulm University, which is funded by Ministry for Science, Research and Arts of the State of Baden-Württemberg, Germany.