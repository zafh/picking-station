package PredictionServerAgent;

import jadex.bridge.service.annotation.Timeout;
import jadex.commons.future.IFuture;

public interface IPredictionService {
	
	@Timeout(Timeout.NONE)
	public IFuture<String> DetectObject(String image, String article);
	
}
