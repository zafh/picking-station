package PredictionServerAgent;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class CommandShell extends JFrame {

    private static final long serialVersionUID = 1L;
    
    public Process process;    
	PrintWriter processInput;
    
	public List<String> Output = new ArrayList<String>();
	
    public JTextArea txtConsole;
    public JTextField txtCommand;

    public CommandShell (String signal) {
           	
        super("CommandShell");
        
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    
        txtConsole = new JTextArea(20, 80);
        txtConsole.setEditable(false);
        JScrollPane scrollPane = new JScrollPane(txtConsole); 
        add(scrollPane);
        
        txtCommand = new JTextField(80);
        add(txtCommand, BorderLayout.SOUTH);
    
    	txtCommand.addActionListener(new ActionListener() {              
    		public void actionPerformed(ActionEvent e) {
    			processInput(txtCommand.getText());
    			txtCommand.setText("");
    		}
    	});
        
        pack(); 
	    setVisible(true);   
        
        startShell(signal);
        
    }

    public void startShell(String signal) {
        
        ProcessBuilder processBuilder = new ProcessBuilder("cmd.exe").redirectErrorStream(true);
        
        try {          
        	process = processBuilder.start();
        	processInput = new PrintWriter(process.getOutputStream());      
        	captureProcessOutput(process, signal); 
        	// process.waitFor();
        	
        } catch (Exception e) {
        	e.printStackTrace();
        }
        
    }
    
    public void processInput(String input) {
    	processInput.println(input);
		processInput.flush();
    }

  	public void captureProcessOutput(final Process process, final String signal) {
      
  		Output.clear();
  		
  		Executors.newSingleThreadExecutor().execute(new Runnable() {
  			public void run() {
  		
			  	String line = "";
			    char c = (char)-1;
				
			    InputStream processOutput = process.getInputStream();
			      
			    try {
					while ((c = (char) processOutput.read()) != -1) { 
						txtConsole.append(String.valueOf(c));
	                    txtConsole.setCaretPosition(txtConsole.getText().length());
						line = line + String.valueOf(c);                    
					    if (Character.getType(c) == 15) {
					    	Output.add(line);
					    	if (line.contains(signal)) {
					    		processInput.close();
					    		processOutput.close();
					    		process.destroy();
					    		break;
					    	}
					    	line = "";
					    }
					}
				} catch (IOException e) {
					e.printStackTrace();
				}

  			}  	      
  		});
	    
        
  	}

}