package PredictionServerAgent;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import jadex.bdiv3.annotation.Goal;
import jadex.bdiv3.annotation.GoalParameter;
import jadex.bdiv3.annotation.GoalResult;
import jadex.bdiv3.annotation.GoalTargetCondition;
import jadex.bdiv3.annotation.Plan;
import jadex.bdiv3.annotation.RawEvent;
import jadex.bdiv3.annotation.Trigger;
import jadex.bdiv3.features.IBDIAgentFeature;
import jadex.bdiv3.runtime.ChangeEvent;
import jadex.bridge.IComponentStep;
import jadex.bridge.IInternalAccess;
import jadex.bridge.component.IExecutionFeature;
import jadex.commons.future.Future;
import jadex.commons.future.IFuture;
import jadex.micro.annotation.Agent;
import jadex.micro.annotation.AgentArgument;
import jadex.micro.annotation.AgentBody;
import jadex.micro.annotation.AgentCreated;
import jadex.micro.annotation.AgentKilled;
import jadex.micro.annotation.Argument;
import jadex.micro.annotation.Arguments;
import jadex.micro.annotation.ProvidedService;
import jadex.micro.annotation.ProvidedServices;

@Agent
@Arguments({
	@Argument(name="argPathDarknet", clazz=String.class),
	@Argument(name="argPathData", clazz=String.class),
	@Argument(name="argPathConfig", clazz=String.class),
	@Argument(name="argPathWeights", clazz=String.class),
	@Argument(name="argThresholdObjectDetection", clazz=Double.class)
	})
@ProvidedServices({
	@ProvidedService(type=IPredictionService.class)
})
public class PredictionServerAgentBDI implements IPredictionService {
	
/*	

try {
	
	OutputStream output = socket.getOutputStream();
    PrintWriter writer = new PrintWriter(output, true);
	
    JsonWriter JsonWriter;
	JsonWriter = new JsonWriter(writer);
	
	...
	
	JsonWriter.close();
	
} catch (IOException ex) {
	
	System.out.println("Server exception: " + ex.getMessage());
    ex.printStackTrace();
    
}

---------------------------------------------------------------------------------

PickingStatio-Agent -> Photoneo System
{
  "performative" : "request",
  "id" : < message id of request >,
  "type" : "picking order",
  "content" : {
      "product" : < product id or name of product >,
      "destination" : < coordinates to place the picked product >
  }
}

JsonWriter.beginObject();
JsonWriter.name( "performative" ).value( "request" );
JsonWriter.name( "id" ).value( <String> );
JsonWriter.name( "type" ).value( "picking order" );
JsonWriter.name( "content" );
	JsonWriter.beginObject();
	JsonWriter.name( "product" ).value( <String> );
	JsonWriter.name( "destination" ).value( <String> );            	
	JsonWriter.endObject();
JsonWriter.endObject();

---------------------------------------------------------------------------------

Photoneo System -> PredictionServer-Agent
{
  "performative" : "request",
  "id" : < message id of request >,
  "type" : "object detection",
  "content" : {
      "product" : < product id or name of product >,
      "shape" : {
        "X" : < width of image in pixel >,
        "Y" : < heigth of image in pixel >
      },
      "image" : <network path [string] or image data >
  }
}


---------------------------------------------------------------------------------

PredictionServer-Agent -> Photoneo System
{
  "performative" : "response",
  "id" : < message id of coresponding request >,
  "content" : {
      "results" : < number of results = dimension of arrays >,
	  "scores" : [ < value1 > ,
	       		   < value2 > ,
	        	   ... ],
	  "boxes" : [ [ <X1>, <Y1>, <Width1>, <Heigth1> ],
	              [ <X2>, <Y2>, <Width2>, <Heigth2> ],
	              [ ... ]],
	  "masks" : [{ < Matrix1 [Array of Array] > },
	             { < Matrix2 [Array of Array] > },
	             { ... }
	            ]
  }
}

JsonWriter.beginObject();
JsonWriter.name( "performative" ).value( "response" );
JsonWriter.name( "id" ).value( <String> );
JsonWriter.name( "content" );
	JsonWriter.beginObject();
	JsonWriter.name( "results" ).value( <Integer> );
	JsonWriter.object().key("results" ).array();
		for {
			JsonWriter.value( <Double> );
		}
		JsonWriter.endArray().endObject();
	...	
	JsonWriter.endObject();
JsonWriter.endObject();

---------------------------------------------------------------------------------

Photoneo System -> PickingStation-Agent
{
  "performative" : "response"
  "id" : < message id of coresponding request >
  "content" : {
      "code" : < code to confirm picking or to refer to an error >
  }
}
	
*/	

	//-------- arguments --------
	
	@AgentArgument
	protected String argPathDarknet;
	
	@AgentArgument
	protected String argPathData;
	
	@AgentArgument
	protected String argPathConfig;
	
	@AgentArgument
	protected String argPathWeights;

	@AgentArgument
	protected double argThresholdObjectDetection;
	
	//-------- parameters --------
	
	@Agent
	protected IInternalAccess Agent;
	protected IExecutionFeature execFeature;
	protected String AgentName;
	
	protected String PathDarknet;
	protected String PathData;
	protected String PathConfig;
	protected String PathWeights;	
	protected double ThresholdObjectDetection;
	
	final static int ImageSizeX = 1280;
	final static int ImageSizeY = 960;

	//-------- beliefs ---------
	
	//-------- creation --------
	
	@AgentCreated
	public void agentCreated() throws InvocationTargetException, InterruptedException {
		
		// read arguments
		PathDarknet = argPathDarknet;
		PathData = argPathData;
		PathConfig = argPathConfig;
		PathWeights = argPathWeights;
		ThresholdObjectDetection = argThresholdObjectDetection;	

		// set local name
		AgentName = Agent.getComponentIdentifier().getLocalName();
		
		// start socket server
		//nRunnable SocketServer = StartServer( 30042 );
		// EventQueue.invokeAndWait(SocketServer);
		
		// System.out.println( "PredictionServer-Agent has been started.");
		
	}	
	
	//-------- body --------
	
	@AgentBody
	public void agentBody() {}
	
	//-------- termination --------
	
	@AgentKilled
	public void agentKilled() {}
		
	//-------- goals --------
	
	@Goal
	public class Request {
		
		@GoalParameter
		protected String Detection = "";
		
		protected String Image;
		protected String Article;
		protected String Type;
		
		public Request(String image, String article, String type) {
			this.Image = image;
			this.Article = article;
			this.Type = type;
		}
		
		public String GetArticle() {
			return this.Article;
		}
		
		public String GetImage() {
			return this.Image;
		}
		
		public void SetDetection(String detection) {
			this.Detection = detection;
		}
		
		@GoalResult
		protected String getResult() {
			
			String response = "";
			
			switch(Type) {			
				case "Service":			
					response = Detection;				
				case "Socket":
					response = Detection;				
			}
			
			return response;

		}
		
		@GoalTargetCondition(rawevents=@RawEvent(ChangeEvent.PARAMETERCHANGED))
		public boolean checkTarget() {
			return !Detection.equals("");
		}

	}
	
	//-------- plans ----------
		
	@Plan(trigger=@Trigger(goals=Request.class))
    protected void Process(Request goal) throws InterruptedException {
		
		String image = goal.GetImage();
		String article = goal.GetArticle();

		// System.out.println( "Start processing request..." );
		
		List<String> ResultDetection = DarktnetYolo( image, article );
		
		double prop = 0.0;
		String detection = "";
		
		// get article with highest propability of object detecetion
		for (String entry : ResultDetection) {		
			if (Double.valueOf(entry.split(",")[0]) > prop) {
				prop = Double.valueOf(entry.split(",")[0]);
				detection = entry;
			}
		}
		
		// int[] mask = CalculateMask(0, 0, box);
		
		// return result
		if ( prop >= ThresholdObjectDetection ) {
			// System.out.println("Object detection successful:");
			goal.SetDetection( detection );
		} else {
			// System.out.println("Object detection not successful!");
			goal.SetDetection( "NIX" );
		}
	
	}
			
	//-------- methods --------

	public int[] CalculateMask(int width, int height, String box) {
		
		/* Diese Methode muss vermutlich noch einmal überarbeitet werden:
		 * - Ist die Maske wirklich ein Flat-Array oder ein Array of Array?
		 * - Ist die Richtung der Koordinaten (x: left->right, y: bottom->top) so korrekt?
		 */
		
		int c = 0;
		int[] mask = new int[width*height];	
		String[] value = box.split(",");
		
		int left = (int) (Integer.valueOf(value[0]) - Math.ceil(Double.valueOf(value[2])*width*0.5)); // CenterX - Width/2
		int right = (int) (Integer.valueOf(value[0]) + Math.ceil(Double.valueOf(value[2])*width*0.5)); // CenterX + Width/2
		int top = (int) (Integer.valueOf(value[1]) + Math.ceil(Double.valueOf(value[3])*height*0.5)); // CenterY + Height/2
		int bottom = (int) (Integer.valueOf(value[1]) - Math.ceil(Double.valueOf(value[3])*height*0.5)); // CenterY - Height/2
		
		for (int x=0; x<width; x++) {
			for (int y=0; y<height; y++) {
				
				if (x>left && x<right && y>bottom && y<top ) {
					mask[c] = 1;
					c++;
				} else {
					mask[c] = 0;
					c++;
				}
	
			}
		}
		
		return mask;
	}
	
    public Runnable StartServer(int port) {
    	
    	Runnable Run = new Runnable() {  		
    		public void run() {

		        try (ServerSocket serverSocket = new ServerSocket(port)) {
		        	
		        	while (true) {
		        		
						try {
							
							Socket socket = serverSocket.accept();

							execFeature.scheduleStep(new IComponentStep<Void>() {

								public IFuture<Void> execute(IInternalAccess arg0) {

									 try {
								        	
							            InputStream input = socket.getInputStream();
							            BufferedReader reader = new BufferedReader(new InputStreamReader(input));
							 
							            OutputStream output = socket.getOutputStream();
							            @SuppressWarnings("unused")
										PrintWriter writer = new PrintWriter(output, true);

							            while (true) {
							            
								            char[] buffer = new char[1200];
								            reader.read(buffer);
								            String message = new String(buffer);
								
								            // "Object Detection"
								            GsonBuilder builder = new GsonBuilder();
								            Gson gson = builder.create();
											@SuppressWarnings({ "unchecked", "unused" })
											Map<String, Object> json = gson.fromJson(message, Map.class);
											
											// create request and wait for response
											// String image = (String) json.get("image");
											// String detection = (String) Agent.getComponentFeature(IBDIAgentFeature.class).dispatchTopLevelGoal( new Request( image, article, "Socket" ) ).get();
											// String data[] = detection.split(",");
								
								            // JsonWriter JsonWriter;
											// JsonWriter = new JsonWriter(null);
											// ...			
											// writer.println(JsonWriter.toString());
											// writer.flush();
								            
							            }
							    
								        } catch (IOException ex) {
								            System.out.println("Server exception: " + ex.getMessage());
								            ex.printStackTrace();
								        }
									
									return null;
								}
 
							});
							
						} catch (IOException e) {
							e.printStackTrace();
						}    
						
		        	}
		 
		        } catch (IOException ex) {
		        	
		            System.out.println("Server exception: " + ex.getMessage());
		            ex.printStackTrace();
		            
		        }
        
    		}     		
    	};

    	return Run;
    	
    }
	
	public List<String> DarktnetYolo(String image, String article) throws InterruptedException {
		
		CommandShell cs = new CommandShell("End of Object Detection");	
		
		String cmdChangeDirectory = "cd " + PathDarknet;
		cs.processInput(cmdChangeDirectory);
		
		String cmdObjectDetection = "darknet_no_gpu.exe detector test " + PathData + " " + PathConfig + " " + PathWeights + " -ext_output " + image;
		cs.processInput(cmdObjectDetection);

		cs.process.waitFor();
		
		List<String> result = new ArrayList<String>();
		
		double prop = 0.0;
		String box = "";
		
		for (int i=0; i<cs.Output.size(); i++) {
							
			if (cs.Output.get(i).contains(article)) {
				
				// line CMD --> Raspberry-Box: 100%
				double value = Double.valueOf(cs.Output.get(i).split(":")[1].replace("%", ""));
				
				if (value > prop) {
					// update best result
					prop = value;
					// process entry
						// line CMD --> (left_x:  628   top_y:  225   width:  118   height:  158)
					String data = cs.Output.get(i+1).replace("(", "").replace(")", "").replace("\r", "");
						// --> data = "left_x:  628   top_y:  225   width:  118   height:  158"
					String[] BB = data.replace("  ", "").replace(" ", ":").split(":");
						// --> bb = [left_x,628,top_y,225,width,118,height,158]
					// Formatierung in Yolo-Format
						// --> Verschiebung X-/Y-Koordinaten
					BB[1] = String.valueOf(Double.valueOf(BB[1]) + Math.ceil(Double.valueOf(BB[5])/2));
					BB[3] = String.valueOf(Double.valueOf(BB[3]) + Math.ceil(Double.valueOf(BB[7])/2));
						// --> Normierung
					double[] normBB = new double[4];
					normBB[0] = Double.valueOf(BB[1]) / ImageSizeX;
					normBB[1] = Double.valueOf(BB[3]) / ImageSizeY;
					normBB[2] = Double.valueOf(BB[5]) / ImageSizeX;
					normBB[3] = Double.valueOf(BB[7]) / ImageSizeY;
					// set return value
					box = String.valueOf(normBB[0]) + "," + String.valueOf(normBB[1]) + "," + String.valueOf(normBB[2]) + "," + String.valueOf(normBB[3]);
				}
				
				result.add(String.valueOf(value) + "," + box);
	
			}	
			
		}
		
		cs.dispose();
		
		return result;
	
	}
	
	//-------- services --------
	
	public IFuture<String> DetectObject(String image, String article) {

		final Future<String> value = new Future<String>();
		
		// create request and wait for response "P, PosX, PosY, LengthX, LengthY"
		String detection = (String) Agent.getComponentFeature(IBDIAgentFeature.class).dispatchTopLevelGoal( new Request(image, article, "Service") ).get();
		
		value.setResult(detection);		
		return value;

	}
	
}