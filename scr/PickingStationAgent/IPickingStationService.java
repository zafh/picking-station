package PickingStationAgent;

import jadex.bridge.service.annotation.Timeout;
import jadex.commons.future.IFuture;

public interface IPickingStationService {
	
	@Timeout(Timeout.NONE)
	public IFuture<Boolean> AssignPickingOrder(PickingOrder order);
	
}
