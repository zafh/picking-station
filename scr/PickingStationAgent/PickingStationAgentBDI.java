package PickingStationAgent;

import java.awt.EventQueue;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;
import com.google.gson.Gson;
import PredictionServerAgent.IPredictionService;
import RoboceptionAgent.IRoboceptionService;
import UR5eAgent.IUR5eService;
import jadex.bridge.service.RequiredServiceInfo;
import jadex.bridge.service.component.IRequiredServicesFeature;
import jadex.bdiv3.annotation.Belief;
import jadex.bdiv3.annotation.Goal;
import jadex.bdiv3.annotation.GoalMaintainCondition;
import jadex.bdiv3.annotation.GoalParameter;
import jadex.bdiv3.annotation.GoalRecurCondition;
import jadex.bdiv3.annotation.GoalResult;
import jadex.bdiv3.annotation.GoalTargetCondition;
import jadex.bdiv3.annotation.Plan;
import jadex.bdiv3.annotation.PlanAPI;
import jadex.bdiv3.annotation.PlanBody;
import jadex.bdiv3.annotation.PlanPrecondition;
import jadex.bdiv3.annotation.RawEvent;
import jadex.bdiv3.annotation.Trigger;
import jadex.bdiv3.features.IBDIAgentFeature;
import jadex.bdiv3.model.MProcessableElement.ExcludeMode;
import jadex.bdiv3.runtime.ChangeEvent;
import jadex.bdiv3.runtime.IPlan;
import jadex.bdiv3.runtime.impl.RPlan;
import jadex.bridge.IInternalAccess;
import jadex.commons.Tuple2;
import jadex.commons.future.Future;
import jadex.commons.future.IFuture;
import jadex.micro.annotation.Agent;
import jadex.micro.annotation.AgentArgument;
import jadex.micro.annotation.AgentBody;
import jadex.micro.annotation.AgentCreated;
import jadex.micro.annotation.AgentKilled;
import jadex.micro.annotation.Argument;
import jadex.micro.annotation.Arguments;
import jadex.micro.annotation.Binding;
import jadex.micro.annotation.ProvidedService;
import jadex.micro.annotation.ProvidedServices;
import jadex.micro.annotation.RequiredService;
import jadex.micro.annotation.RequiredServices;
import net.coobird.thumbnailator.Thumbnails;

@Agent
@Arguments({
	@Argument(name="argArticles", clazz=List.class),
	@Argument(name="argPathShelfReference", clazz=String.class),
	@Argument(name="argPathYoloMark", clazz=String.class),
	@Argument(name="argMqttBroker", clazz=String.class)
	})
@ProvidedServices({
	@ProvidedService(type=IPickingStationService.class)
	})
@RequiredServices({
	@RequiredService(name="PredictionService", type=IPredictionService.class, multiple=true, binding=@Binding(scope=RequiredServiceInfo.SCOPE_PLATFORM)),
	@RequiredService(name="RoboceptionService", type=IRoboceptionService.class, multiple=true, binding=@Binding(scope=RequiredServiceInfo.SCOPE_PLATFORM)),
	@RequiredService(name="UR5eService", type=IUR5eService.class, multiple=true, binding=@Binding(scope=RequiredServiceInfo.SCOPE_PLATFORM))
	})
public class PickingStationAgentBDI implements IPickingStationService {
		
	//-------- arguments --------
	
	@AgentArgument 
	protected List<Article> argArticles;
	
	@AgentArgument
	protected String argPathShelfReference;
	
	@AgentArgument
	protected String argMqttBroker;
	
	@AgentArgument
	protected String argPathYoloMark;
	
	//-------- parameters --------

	@Agent
	protected IInternalAccess Agent;
	
	protected PickingStationWindowMain windowMain;
	protected PickingStationWindowNewOrder windowNewOrder;	
	protected PickingStationWindowEmergencyCall windowEmergencyCall;
	
	protected IPredictionService PredictionService;
	protected IRoboceptionService RoboceptionService;
	protected IUR5eService UR5eService;
	
	protected String MqttBroker;
	protected MqttClient MqttClient;
	
	protected boolean Working;
	
	protected String PathShelfReference;
	protected List<Shelf> ShelfReference;
	
	protected String PathYoloMark;
	protected int EmergencyCallCounter = 0;
	
	final static int IMAGE_SIZE = 600;		
	final static double SuctionSurfaceLength = 0.02;
	final static double SuctionSurfaceWidth = 0.02;
	
	//-------- beliefs ---------
	
	@Belief
	protected List<Article> Articles = new ArrayList<Article>();
	
	@Belief
	protected List<PickingOrder> PickingOrders = new ArrayList<PickingOrder>();
	
	@Belief
	protected boolean ControlSwitch = false;
	
	@Belief
	protected boolean HumanSupport = false;
	
	//-------- creation --------
	
	@AgentCreated
	public void agentCreated() throws InvocationTargetException, InterruptedException, IOException {
				
		// load arguments	
		Articles.addAll(argArticles);		
		PathShelfReference = argPathShelfReference;
		PathYoloMark = argPathYoloMark;
		MqttBroker = argMqttBroker;
		
		// import shelf data
		ImportShelfReference();
		
		// start MQTT client
		StartMqttClient();
			
		// create GUI
		EventQueue.invokeAndWait(new Runnable() {	
			public void run() {
				// create window and add action and event listeners
				windowMain = new PickingStationWindowMain();
				ConfigureWindowMain();
			}
		});
		windowMain.frmPickingStation.setVisible(true);
		
		// create GUI for creating new picking order
		EventQueue.invokeAndWait(new Runnable() {	
			public void run() {
				// create window and add action and event listeners
				windowNewOrder = new PickingStationWindowNewOrder();
				ConfigureWindowNewOrder();
			}
		});
		
		// create GUI for Emergency Call
		EventQueue.invokeLater(new Runnable() {	
			public void run() {
				windowEmergencyCall = new PickingStationWindowEmergencyCall();
				ConfigureWindowEmergencyCall();
			}
		});
		
		windowMain.taStatus.append("PickingStation-Agent has been started. \n");
		
	}	
		
	//-------- body --------
	
	@AgentBody
	public void agentBody() {
		
		// get services
		windowMain.taStatus.append("Search for Prediction-Service... ");
		PredictionService = (IPredictionService) Agent.getComponentFeature(IRequiredServicesFeature.class).getRequiredService("PredictionService").get();	
		windowMain.taStatus.append("Connection has been established! \n");
		windowMain.taStatus.append("Search for Roboception-Service... ");
		RoboceptionService = (IRoboceptionService) Agent.getComponentFeature(IRequiredServicesFeature.class).getRequiredService("RoboceptionService").get();
		windowMain.taStatus.append("Connection has been established! \n");
		windowMain.taStatus.append("Search for UR5e-Service... ");
		UR5eService = (IUR5eService) Agent.getComponentFeature(IRequiredServicesFeature.class).getRequiredService("UR5eService").get();
		windowMain.taStatus.append("Connection has been established! \n");
		
		windowMain.taStatus.append("\n");
		Working = false;
		Agent.getComponentFeature(IBDIAgentFeature.class).dispatchTopLevelGoal(new FinishPickingOrders());
	
	}
	
	//-------- termination --------
	
	@AgentKilled
	public void agentKilled() {}
		
	//-------- goals --------
	
	@Goal(excludemode=ExcludeMode.Never, unique=true) 
	public class FinishPickingOrders {
		
		@GoalMaintainCondition(beliefs={"PickingOrders"})
		public boolean checkMaintain() {
			return PickingOrders.size() == 0;
		}
	
	}	

	@Goal(unique=true)
	public class DetectObject {
		
		@GoalParameter
		protected String Detection;
		
		protected String Article;
		protected String Image;
		protected int EmergencyCall;
		
		public DetectObject(String image, String article) {
			this.Detection = "";
			this.Image = image;
			this.Article = article;
			this.EmergencyCall = 0;
		}
		
		public String getArticle() { return this.Article; }		
		public int getEmergencyCall() { return this.EmergencyCall; }
		public String getImage() { return this.Image; }
		
		public void setEmergencyCall(int level) { this.EmergencyCall = level; }	
		public void setDetection(String detection) { this.Detection = detection; }
		public void setImage(String image) { this.Image = image; }
	
		@GoalResult
		protected String getResult() {
			return this.Detection;
		}
		
		@GoalTargetCondition(rawevents=@RawEvent(ChangeEvent.PARAMETERCHANGED))
		public boolean checkTarget() {
			return !this.Detection.equals("");
		}
				
	}
	
	@Goal(recur=true)
	public class WaitForFeedback {
		
		@GoalRecurCondition(beliefs="ControlSwitch")
		public boolean checkRecur() {
		  return true;
		}		

		boolean value;
		
		public WaitForFeedback(boolean value) {
			this.value = value;
		}
		
		@GoalResult
		protected boolean getResult() {
			return true;
		}
		
		@GoalTargetCondition(beliefs={"ControlSwitch"})
		public boolean checkTarget() {
			return ControlSwitch != this.value;
		}
	
	}
	
	@Goal (recur=true)
	public class HumanSupport {
	
		@GoalRecurCondition(beliefs="HumanSupport")
		public boolean checkRecur() {
		  return true;
		}
		
		@GoalResult
		protected boolean getResult() {
			return true;
		}
		
		@GoalTargetCondition(beliefs="HumanSupport")
		public boolean checkTarget() {
			if (HumanSupport == true) {
				return true;
			} else {
				return false;
			}
		}
			
	}
	
	//-------- plans ----------
	
	@Plan(trigger=@Trigger(goals=FinishPickingOrders.class))
	public class Picking {
	
		@PlanAPI
		RPlan plan;
		
		@PlanPrecondition
		protected Boolean PlanPrecondition() {
		    return PickingOrders.size() != 0;
		}

		@PlanBody
		public void Body() throws IOException, InterruptedException {
			
			// return if another order is processed
			if (Working == true)
				return;
			
			PickingOrder order = PickingOrders.get(0);
			windowMain.taStatus.append("Start processing order " + order.getOrderID() + "... \n");
			Working = true;
			
			for (Tuple2<Article,Integer> position : order.getPositions()) {			
				for (int i=0; i<position.getSecondEntity(); i++) {

					// get image
					windowMain.taStatus.append("Record image..."); 
					String image = RoboceptionService.RecordImage().get();
					windowMain.taStatus.append("Image has been stored! \n");
					
					// detect object
					windowMain.taStatus.append("Detect object...");
					DetectObject goal = new DetectObject(image, position.getFirstEntity().getName() );
					String detection = (String) plan.dispatchSubgoal(goal).get();
					windowMain.taStatus.append("Bounding Box has been received! \n");
					
					// evaluate roi
					windowMain.taStatus.append("Calculate gripping parameter...");
					String roi = GetROI(detection);
					windowMain.taStatus.append("Region of interest has been calculated! \n");
					
					// get gripping point (grasp)
					windowMain.taStatus.append("Prepare picking...");
					String grasp = "";
					for (int c=0; c<3; c++) {
						Map<String,Object> parameter = new HashMap<String,Object>();
							parameter.put("pose_frame", "external");
							parameter.put("region_of_interest_id", roi);
							parameter.put("suction_surface_length", SuctionSurfaceLength);
							parameter.put("suction_surface_width", SuctionSurfaceWidth);
						grasp = RoboceptionService.ComputeGrasp(parameter).get();
						if (!grasp.equals("")) 
							break;
					}
					windowMain.taStatus.append("Gripping point has been calculated! \n");

					// pick object
					windowMain.taStatus.append("Start picking...");
					UR5eService.PickObject(grasp).get();
					windowMain.taStatus.append("Object has been placed! \n");
					
				}
			}
			
			// finish order
			RemoveOrder(order);
			windowMain.taStatus.append("Order " + order.getOrderID() + " has been finished! \n");
			windowMain.taStatus.append("\n");
			Working = false;
			
		}
		
	}
	
	@Plan(trigger=@Trigger(goals=DetectObject.class), priority=10)
	protected void CallService(DetectObject goal) throws InterruptedException {

		String detection = "Kein Objekt gefunden!"; //PredictionService.DetectObject( goal.getImage(), goal.getArticle() ).get();		
		
		if (!detection.equals("Kein Objekt gefunden!"))
			goal.setDetection(detection);

	}
	
	@Plan(trigger=@Trigger(goals=DetectObject.class), priority=6)
	protected void SupportLevelOne(DetectObject goal, IPlan plan) throws InterruptedException, IOException, MqttPersistenceException, MqttException {
				
		// support level 1: call human support and adjust environment
		
		BufferedImage bufferedImage;
		BufferedImage resizedicon;
		ImageIcon icon;
		
		// show gui
		windowEmergencyCall.lblArticle.setText(goal.getArticle());
	    windowEmergencyCall.frmEmergencyCall.setVisible(true);	    
		windowEmergencyCall.taStatus.append("Start Support Level 1: \r\n");

		// search for human picker
		windowEmergencyCall.taStatus.append("Search for human picker");
	   	String topic = "EmergencyCall";
		String message = "PickingStation got stuck while object detection and needs your help!";		
		MqttClient.publish(topic, new MqttMessage(message.getBytes()));
		HumanSupport = false;
		plan.dispatchSubgoal(new HumanSupport()).get();
		windowEmergencyCall.taStatus.append(" - DONE \r\n");

		// import and show current view
		windowEmergencyCall.taStatus.append("Record picture");
		String path = goal.getImage();			
		File img = new File(path);
		bufferedImage = ImageIO.read(img);	
		resizedicon = Thumbnails.of(bufferedImage).size(IMAGE_SIZE,IMAGE_SIZE).asBufferedImage();
		icon = new ImageIcon(resizedicon);
		windowEmergencyCall.lblPicture.setIcon(icon);			
		windowEmergencyCall.btnEnvironmentModified.setEnabled(true);
		windowEmergencyCall.taStatus.append(" - DONE \r\n");
			
		// wait for adjustment
		windowEmergencyCall.taStatus.append("Wait for adjustment of environment");
		plan.dispatchSubgoal(new WaitForFeedback(ControlSwitch)).get();
		windowEmergencyCall.btnEnvironmentModified.setEnabled(false);
		windowEmergencyCall.taStatus.append(" - OK \r\n");	
		
		// import and show current view		
		windowEmergencyCall.taStatus.append("Record picture");
		String image = RoboceptionService.RecordImage().get();
		goal.setImage(image);
		img = new File(image);
		bufferedImage = ImageIO.read(img);
		resizedicon = Thumbnails.of(bufferedImage).size(IMAGE_SIZE,IMAGE_SIZE).asBufferedImage();
		icon = new ImageIcon(resizedicon);
		windowEmergencyCall.lblPicture.setIcon(icon);							
		windowEmergencyCall.taStatus.append(" - DONE \r\n");
		windowEmergencyCall.btnRetryObjectDetection.setEnabled(true);
		
		// wait for confirmation to restart object detection
		windowEmergencyCall.taStatus.append("Wait for trigger");
		plan.dispatchSubgoal(new WaitForFeedback(ControlSwitch)).get();
		windowEmergencyCall.btnRetryObjectDetection.setEnabled(false);
		windowEmergencyCall.taStatus.append(" - OK \r\n");
			
		// restart object detection
		windowEmergencyCall.taStatus.append("Restart object detection");		
		String detection = "Kein Objekt gefunden!"; //PredictionService.DetectObject( goal.getImage(), goal.getArticle() ).get();		

		if (!detection.equals("Kein Objekt gefunden!")) {
			windowEmergencyCall.taStatus.append(" - OK \r\n");
			windowEmergencyCall.btnReleaseRobot.setEnabled(true);
			plan.dispatchSubgoal(new WaitForFeedback(ControlSwitch)).get();
			windowEmergencyCall.btnReleaseRobot.setEnabled(false);
			// reset window
			windowEmergencyCall.taStatus.setText("");
			windowEmergencyCall.lblArticle.setText("");
			windowEmergencyCall.lblPicture.setIcon(null);
			windowEmergencyCall.frmEmergencyCall.setVisible(false);
			goal.setDetection(detection);
		}
					
	}
	
	@Plan(trigger=@Trigger(goals=DetectObject.class), priority=4)
	protected void SupportLevelTwo(DetectObject goal, IPlan plan) throws IOException, InterruptedException {
		
		// support level 2: human support marks object
		
		windowEmergencyCall.taStatus.append(" - FAILED \r\n");
		windowEmergencyCall.taStatus.append("\r\n");
		windowEmergencyCall.taStatus.append("Start Support Level 2: \r\n");

		windowEmergencyCall.taStatus.append("Start YOLO Mark");	
				
		// create new directory for Emergency Call
		EmergencyCallCounter++;
		File Dir = new File(PathYoloMark + "\\EC_PickingStation_" + String.valueOf(EmergencyCallCounter));				
		Dir.mkdir();
		// create new folder img with current picture
		File ImgDir = new File(Dir + "\\img");
		ImgDir.mkdir();
		Path sourceDir = Paths.get(goal.getImage());
		Path targetDir = Paths.get(ImgDir + "\\image.png");
		Files.copy(sourceDir, targetDir, StandardCopyOption.REPLACE_EXISTING);
		// create new train.txt
		File train = new File(Dir + "\\train.txt");
		train.createNewFile();
		// create new obj.names
		File names = new File(Dir + "\\obj.names");
		names.createNewFile();
		BufferedWriter bw = new BufferedWriter(new FileWriter(Dir + "\\obj.names"));
		bw.write(goal.getArticle());
		bw.close();

		// start of yolo_mark.exe (OpenCV)
		CommandShell cs = new CommandShell("txt_filename_path"); // <-- Das funktioniert, weil nur ein einziges Bild mit Yolo Mark annotiert wird!

		String cmdChangeDirectory = "cd " + PathYoloMark;
		cs.processInput(cmdChangeDirectory);
		
		String cmdObjectAnnotation = "yolo_mark.exe " + Dir + "\\img " + Dir + "\\train.txt " + Dir + "\\obj.names";
		cs.processInput(cmdObjectAnnotation);	
		cs.process.waitFor();
		
		// get result from file refered in last line: "txt_filename_path = PATH"		
		String file = cs.Output.get(cs.Output.size()-1).split(" = ")[1];
		file = file.replace("/", "\\");
		file = file.replace("\r", "");
		cs.dispose();
		@SuppressWarnings("resource")
		BufferedReader reader = new BufferedReader( new FileReader(file) );
		String line[] = reader.readLine().split(" ");
		String detection = "100.00, " + line[1] + ", " + line[2] + ", " + line[3] + ", " + line[4]; 
		
		
		windowEmergencyCall.taStatus.append(" - DONE \r\n");	
		windowEmergencyCall.btnReleaseRobot.setEnabled(true);
		plan.dispatchSubgoal(new WaitForFeedback(ControlSwitch)).get();
		windowEmergencyCall.btnReleaseRobot.setEnabled(false);
		
		// reset window
		windowEmergencyCall.taStatus.setText("");
		windowEmergencyCall.lblArticle.setText("");
		windowEmergencyCall.lblPicture.setIcon(null);
		windowEmergencyCall.frmEmergencyCall.setVisible(false);

		goal.setDetection(detection);

	}
	
	//-------- methods --------
	
	public void ImportShelfReference() throws IOException {
		
		// Es gibt irgendeine Wechselwirkung zwischen der Methode fromJson und Jadex.
		// Daher wird die Json-Datei als Array ausgelesen und dann in eine Liste transformiert.
		
		ShelfReference = new ArrayList<Shelf>();
		BufferedReader reader = new BufferedReader( new FileReader("C:\\PickingStation\\ShelfReference.json") );

		String myJson = "";
		String line;
		while ((line = reader.readLine()) != null)
            myJson = myJson + line;

		Gson gson = new Gson(); 
		Shelf[] shelfArray = gson.fromJson(myJson, Shelf[].class);
		
		for (Shelf shelf : shelfArray)
			ShelfReference.add(shelf);
		
		reader.close();	
	
	}
	
	public void AssignOrder(PickingOrder order) {
		PickingOrders.add(order);
		windowMain.lmAuftr�ge.addElement(order.getOrderID());
		windowMain.frmPickingStation.repaint();
	}
	
	public void RemoveOrder(PickingOrder order) {
		PickingOrders.remove(order);
		// if necessary remove selection of order
		if ( order.getOrderID().equals(windowMain.listAuftr�ge.getSelectedValue()) )
			windowMain.listAuftr�ge.setSelectedIndex(-1);
		for (int i=0; i<windowMain.lmAuftr�ge.size(); i++) {
			if (windowMain.lmAuftr�ge.get(i).equals(order.getOrderID())) {
				windowMain.lmAuftr�ge.remove(i);
				break;
			}		
		}
		windowMain.frmPickingStation.repaint();
	}
	
	public String GetROI(String detection) {
		
		String roi = "";
		double best = 0.0;
		double overlap;
		
		for (Shelf shelf : ShelfReference) {			
			overlap = CalculateOverlap(detection, shelf);			
			if ( overlap > best ) {
				best = overlap;
				roi = shelf.Name;
			}
		}

		return roi;
		
	}
	
	public static double CalculateOverlap(String detection, Shelf shelf) {
		
		double overlap = 0.0;
		double x;
		double y;
		double w;
		double h;
		
		/* Boundingbox Yolo: CenterX, CenterY, Width, Height
		 *                   Origin: Top Left Corner
		 *                   -> Format von detection und shelf
		 *
		 * Rectangle2D: UpperLeftCornerX, UpperLeftCornerY, Width, Height
		 *              Origin: Left Bottom Corne
		 *
		 * Umrechnung: Yolo [x,y,w,h] = Rectangle2D [(x�w/2), 1�(y�h/2), w, h]
		 */

		// set bounding box
		String data[] = detection.split(",");
		h = Double.valueOf(data[4]);
		w = Double.valueOf(data[3]);
		y = Double.valueOf(data[2]) - h/2; //1 - (Double.valueOf(data[2]) - h/2);	
		x = Double.valueOf(data[1]) - w/2;	
		Rectangle2D rectangleBoundingBox = new Rectangle2D.Double();
		rectangleBoundingBox.setFrame(x, y, w, h);
		
		// set shelf		
		h = shelf.Height;
		w = shelf.Width;
		y = shelf.Y - h/2; //1 - (shelf.Y - h/2);	
		x = shelf.X - w/2;
		Rectangle2D rectangleShelf = new Rectangle2D.Double();
		rectangleShelf.setFrame(x, y, w, h);

		// calculate intersection
		Rectangle2D rectangleIntersection = new Rectangle2D.Double();
		Rectangle.intersect(rectangleBoundingBox, rectangleShelf, rectangleIntersection);
		if (rectangleIntersection.getWidth() > 0 && rectangleIntersection.getHeight() > 0)
			overlap = (rectangleIntersection.getWidth() * rectangleIntersection.getHeight()) / (rectangleShelf.getWidth() * rectangleShelf.getHeight());
		
		// https://stackoverflow.com/questions/22062600/how-to-find-the-area-of-intersect-of-recangle2d-in-java
		// double areaShelf = rectangleShelf.getWidth() * rectangleShelf.getHeight();
		// double areaIntersection = rectangleIntersection.getWidth() * rectangleIntersection.getHeight();
		// overlap = (areaShelf == 0 || areaIntersection <= 0) ? 0 : (areaIntersection / areaShelf) * 100;

		return overlap;
		
	}
	
	public void StartMqttClient() {
		
		// connect to MQTT broker
		try { 
			MqttClient = new MqttClient("tcp://" + MqttBroker, Agent.getComponentIdentifier().toString());				 
			MqttConnectOptions options = new MqttConnectOptions();
							   options.setAutomaticReconnect(true);
							   options.setCleanSession(true);
							   options.setConnectionTimeout(10);	
							   options.setUserName("user");
							   options.setPassword("password".toCharArray());
			MqttClient.connect(options);
			// MqttClient.subscribe("EmergencyCall");
			MqttClient.subscribe("PickingStation");
		} catch (MqttException e) { 
			System.out.println(e.getMessage());	
		}
		
		// set callback for incoming messages
		MqttClient.setCallback(new MqttCallback() {
			
	        @Override
	        public void connectionLost(Throwable throwable) { }
	  
	        @Override
	        public void messageArrived(String t, MqttMessage m) throws Exception {
	        	
	        	String payload = new String (m.getPayload());
	        	
	        	// evaluate payload
	        	switch (t) {
	        	
	        		// case "EmergencyCall": 
	        		//	   break;
	        		
	        		case "PickingStation":
	        			String response = payload.trim().toLowerCase();
	        			if (response.contains("ok")) {
	        				String topic = "EmergencyCall";
		        			String message = "Thank you! PickingStation is waiting for your arrival.";		
		        			MqttClient.publish(topic, new MqttMessage(message.getBytes()));
		        			HumanSupport = true;
	        			}
	        			break;
	        			
	        	}
	        	
	        }

			@Override
			public void deliveryComplete(IMqttDeliveryToken arg0) { }
	        
	     });
				
	}
	
	public void ConfigureWindowNewOrder() {
		
		for (Article article : Articles) {
			windowNewOrder.cbArtikel.addItem(article.getName());
		}
		
		windowNewOrder.btnPositionEinf�gen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String Artikel = (String) windowNewOrder.cbArtikel.getSelectedItem();
				int Menge = Integer.valueOf(windowNewOrder.tfAnzahl.getText());	
				String entry = Artikel + " " + Menge;	
				windowNewOrder.lmPositionen.addElement(entry);
			}		
		});
		
		windowNewOrder.btnAbbrechen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				windowNewOrder.frmNewOrder.setVisible(false);
			}		
		});
		
		windowNewOrder.btnAuftragFreigeben.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String ID = windowNewOrder.tfAuftragID.getText();
				PickingOrder order = new PickingOrder(ID);
				
				for (int i=0; i<windowNewOrder.lmPositionen.size(); i++) {
					String[] position = windowNewOrder.lmPositionen.get(i).split(" ");
					for (Article article : Articles) {
						if (article.getName().equals(position[0]))
							order.addPosition(article,Integer.valueOf(position[1]));
					}					
				}

				AssignOrder(order);
				windowNewOrder.frmNewOrder.setVisible(false);
				
			}		
		});
		
	}
	
	public void ConfigureWindowMain() {
		
		windowMain.frmPickingStation.setTitle(Agent.getComponentIdentifier().getLocalName());
		
		windowMain.listAuftr�ge.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent eve) {
								
				String OrderID = windowMain.listAuftr�ge.getSelectedValue();			
				windowMain.tfAuftragID.setText(OrderID);
				
				// search picking order
				PickingOrder pickingorder = null;
				for (PickingOrder order : PickingOrders) {
					if (order.getOrderID().equals(OrderID)) {
						pickingorder = order;
						break;
					}
				}
				
				// show positions
				windowMain.lmPositionen.clear();
				for (Tuple2<Article,Integer> position : pickingorder.getPositions() ) {
					windowMain.lmPositionen.addElement( position.getSecondEntity() + "x " + position.getFirstEntity().getName() );
				}

			}
		});
		
		windowMain.btnAuftrag.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				windowNewOrder.tfAuftragID.setText("Auftrag_" + UUID.randomUUID().toString().substring(0,10));
				windowNewOrder.tfKommissionierer.setText(Agent.getComponentIdentifier().getName());
				windowNewOrder.tf�bergabeplatz.setText(Agent.getComponentIdentifier().getName());
				windowNewOrder.lmPositionen.clear();
				windowNewOrder.frmNewOrder.setVisible(true);
			}		
		});
		
		windowMain.btnConfig.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {			
				// Fenster �ffnen, um Parameter anzupassen.
			}		
		});
	
	}
	
	public void ConfigureWindowEmergencyCall() {
		
		windowEmergencyCall.frmEmergencyCall.setTitle("Emergency Call");
		
		windowEmergencyCall.btnEnvironmentModified.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SetControlSwitch();				
			}
		});
		
		windowEmergencyCall.btnRetryObjectDetection.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SetControlSwitch();	
			}
		});

		windowEmergencyCall.btnReleaseRobot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SetControlSwitch();
			}
		});
		
	}
	
	public void SetControlSwitch() {
		
		if (ControlSwitch == true) 
			ControlSwitch = false;	
	    else 
	    	ControlSwitch = true;		
	}
	
	//-------- services --------
	
	public IFuture<Boolean> AssignPickingOrder(PickingOrder order) {

		final Future<Boolean> value = new Future<Boolean>();
		
		AssignOrder(order);
		
		value.setResult(true);			
		return value;

	}
	
}