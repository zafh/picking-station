package PickingStationAgent;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.DefaultListModel;
import javax.swing.ListSelectionModel;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import java.awt.Color;

public class PickingStationWindowMain {

	public JFrame frmPickingStation;
	
	public JTextField tfAuftragID;
	
	public JTextArea taAufträge;		
	public JTextArea taPositionen;
	public JTextArea taStatus;
	
	public DefaultListModel<String> lmAufträge = new DefaultListModel<String>();
	public JList<String> listAufträge;
	public DefaultListModel<String> lmPositionen = new DefaultListModel<String>();
	public JList<String> listPositionen;
	
	public JButton btnAuftrag;
	public JButton btnConfig;
	
	public PickingStationWindowMain() {
		initialize();
	}

	private void initialize() {
		
		frmPickingStation = new JFrame();
		frmPickingStation.setResizable(false);
		frmPickingStation.setBounds(100, 100, 557, 542);
		frmPickingStation.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel panelAufträge = new JPanel();
		panelAufträge.setBounds(29, 52, 213, 197);
		panelAufträge.setLayout(new BorderLayout(0, 0));
		
		JLabel lblAuftragID = new JLabel("Auftrag-ID");
		lblAuftragID.setBounds(257, 52, 86, 20);
		
		JLabel lblPositionen = new JLabel("Positionen");
		lblPositionen.setBounds(257, 89, 73, 20);
		
		JLabel lblAufträge = new JLabel("Kommissionieraufträge");
		lblAufträge.setBounds(29, 31, 162, 20);
		
		JLabel lblStatus = new JLabel("Systemstatus");
		lblStatus.setBounds(29, 303, 92, 20);
		
		JPanel panelPositionen = new JPanel();
		panelPositionen.setBounds(257, 112, 252, 137);
		panelPositionen.setLayout(new BorderLayout(0, 0));
		
		JPanel panelStatus = new JPanel();
		panelStatus.setBounds(29, 327, 480, 114);
		panelStatus.setLayout(new BorderLayout(0, 0));
		
		tfAuftragID = new JTextField();
		tfAuftragID.setBounds(358, 52, 151, 22);
		tfAuftragID.setBorder(new LineBorder(Color.GRAY));
		tfAuftragID.setEditable(false);
		tfAuftragID.setColumns(10);
		
		btnAuftrag = new JButton("Auftrag anlegen");
		btnAuftrag.setBounds(97, 253, 145, 29);
		
		btnConfig = new JButton("Config");
		btnConfig.setBounds(430, 446, 79, 29);
		
		JScrollPane spStatus = new JScrollPane();
		panelStatus.add(spStatus, BorderLayout.CENTER);
		
		JScrollPane spPositionen = new JScrollPane();
		panelPositionen.add(spPositionen, BorderLayout.CENTER);
		
		taPositionen = new JTextArea();
		taPositionen.setEditable(false);
		spPositionen.setViewportView(taPositionen);
		taPositionen.setRows(5);
		
		taStatus = new JTextArea();
		taStatus.setWrapStyleWord(true);
		taStatus.setEditable(false);
		spStatus.setViewportView(taStatus);
		taStatus.setRows(5);
		
		JScrollPane spAufträge = new JScrollPane();
		panelAufträge.add(spAufträge, BorderLayout.CENTER);
		
		taAufträge = new JTextArea();
		taAufträge.setEditable(false);
		spAufträge.setViewportView(taAufträge);
		taAufträge.setRows(10);
		
		listAufträge = new JList<String>(lmAufträge);
		listAufträge.setBorder(new LineBorder(Color.GRAY));
		listAufträge.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		panelAufträge.add(listAufträge, BorderLayout.CENTER);
		
		listPositionen = new JList<String>(lmPositionen);
		listPositionen.setBorder(new LineBorder(Color.GRAY));
		listPositionen.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		panelPositionen.add(listPositionen, BorderLayout.CENTER);
		frmPickingStation.getContentPane().setLayout(null);
		frmPickingStation.getContentPane().add(btnAuftrag);
		frmPickingStation.getContentPane().add(lblStatus);
		frmPickingStation.getContentPane().add(btnConfig);
		frmPickingStation.getContentPane().add(lblAufträge);
		frmPickingStation.getContentPane().add(panelAufträge);
		frmPickingStation.getContentPane().add(panelPositionen);
		frmPickingStation.getContentPane().add(lblPositionen);
		frmPickingStation.getContentPane().add(lblAuftragID);
		frmPickingStation.getContentPane().add(tfAuftragID);
		frmPickingStation.getContentPane().add(panelStatus);
		
	}
}
