package PickingStationAgent;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.DefaultListModel;
import javax.swing.ListSelectionModel;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import java.awt.Color;
import javax.swing.JComboBox;

public class PickingStationWindowNewOrder {

	public JFrame frmNewOrder;	
	public JTextField tfAuftragID;
	public JTextArea taPositionen;
	public DefaultListModel<String> lmPositionen = new DefaultListModel<String>();
	public JList<String> listPositionen;	
	public JTextField tfKommissionierer;
	public JTextField tfÜbergabeplatz;
	public JTextField tfAnzahl;
	public JComboBox<String> cbArtikel;
	public JButton btnConfig;
	public JButton btnAuftragFreigeben;
	public JButton btnPositionEinfügen;
	public JButton btnAbbrechen;
	
	public PickingStationWindowNewOrder() {
		initialize();
	}

	private void initialize() {
		
		frmNewOrder = new JFrame();
		frmNewOrder.setTitle("Auftrag anlegen");
		frmNewOrder.setResizable(false);
		frmNewOrder.setBounds(100, 100, 423, 412);
		frmNewOrder.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JLabel lblAuftragID = new JLabel("Auftrag-ID");
		lblAuftragID.setBounds(20, 17, 118, 20);
		
		JLabel lblPositionen = new JLabel("Positionen");
		lblPositionen.setBounds(20, 109, 73, 20);
		
		JPanel panelPositionen = new JPanel();
		panelPositionen.setBounds(20, 134, 160, 137);
		panelPositionen.setLayout(new BorderLayout(0, 0));
		
		tfAuftragID = new JTextField();
		tfAuftragID.setBounds(153, 16, 237, 22);
		tfAuftragID.setBorder(new LineBorder(Color.GRAY));
		tfAuftragID.setEditable(false);
		tfAuftragID.setColumns(10);
		
		btnConfig = new JButton("Config");
		btnConfig.setBounds(908, 314, 79, 29);
		
		JLabel lblKommissionierer = new JLabel("Kommissionierer");
		lblKommissionierer.setBounds(20, 47, 118, 20);
		
		JLabel lblÜbergabeplatz = new JLabel("\u00DCbergabeplatz");
		lblÜbergabeplatz.setBounds(20, 79, 101, 20);
		
		tfKommissionierer = new JTextField();
		tfKommissionierer.setBounds(153, 46, 237, 22);
		tfKommissionierer.setEditable(false);
		tfKommissionierer.setColumns(10);
		tfKommissionierer.setBorder(new LineBorder(Color.GRAY));
		
		tfÜbergabeplatz = new JTextField();
		tfÜbergabeplatz.setBounds(153, 78, 237, 22);
		tfÜbergabeplatz.setEditable(false);
		tfÜbergabeplatz.setColumns(10);
		tfÜbergabeplatz.setBorder(new LineBorder(Color.GRAY));
		
		btnAuftragFreigeben = new JButton("Auftrag freigeben");
		btnAuftragFreigeben.setBounds(20, 287, 160, 29);
		
		btnPositionEinfügen = new JButton("Position einf\u00FCgen");
		btnPositionEinfügen.setBounds(190, 134, 157, 29);
		
		cbArtikel = new JComboBox<String>();
		cbArtikel.setBounds(190, 172, 157, 26);
		
		tfAnzahl = new JTextField();
		tfAnzahl.setBounds(356, 172, 34, 26);
		tfAnzahl.setColumns(10);
		
		JScrollPane spPositionen = new JScrollPane();
		panelPositionen.add(spPositionen, BorderLayout.CENTER);
		
		taPositionen = new JTextArea();
		taPositionen.setEditable(false);
		spPositionen.setViewportView(taPositionen);
		taPositionen.setRows(5);
		
		listPositionen = new JList<String>(lmPositionen);
		listPositionen.setBorder(new LineBorder(Color.GRAY));
		listPositionen.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		panelPositionen.add(listPositionen, BorderLayout.CENTER);
		frmNewOrder.getContentPane().setLayout(null);
		frmNewOrder.getContentPane().add(lblPositionen);
		frmNewOrder.getContentPane().add(panelPositionen);
		frmNewOrder.getContentPane().add(btnConfig);
		frmNewOrder.getContentPane().add(btnPositionEinfügen);
		frmNewOrder.getContentPane().add(cbArtikel);
		frmNewOrder.getContentPane().add(btnAuftragFreigeben);
		frmNewOrder.getContentPane().add(tfAnzahl);
		frmNewOrder.getContentPane().add(lblAuftragID);
		frmNewOrder.getContentPane().add(lblKommissionierer);
		frmNewOrder.getContentPane().add(lblÜbergabeplatz);
		frmNewOrder.getContentPane().add(tfÜbergabeplatz);
		frmNewOrder.getContentPane().add(tfKommissionierer);
		frmNewOrder.getContentPane().add(tfAuftragID);
		
		btnAbbrechen = new JButton("Abbruch");
		btnAbbrechen.setBounds(20, 327, 160, 29);
		frmNewOrder.getContentPane().add(btnAbbrechen);
		
	}
}
