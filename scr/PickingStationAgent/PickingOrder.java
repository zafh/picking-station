package PickingStationAgent;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import jadex.bridge.service.annotation.Reference;
import jadex.commons.SimplePropertyChangeSupport;
import jadex.commons.Tuple2;
import jadex.commons.beans.PropertyChangeListener;

@Reference
public class PickingOrder implements Serializable {

	//-------- constants --------
	
	private static final long serialVersionUID = 1L;
	
	public static final String OPEN = "OPEN";
	public static final String ASSIGNED = "ASSIGNED";
	public static final String COMPLETE = "COMPLETE";
	
	//-------- attributes --------
		
	protected String OrderID;	
	
	protected List<Tuple2<Article,Integer>> Positions;
	
	protected String Picker;
	protected String TransferPlace;
	
	protected String Status;
	
	public SimplePropertyChangeSupport pcs;
	
	//-------- constructors --------

	public PickingOrder(String OrderID) {
		
		this.OrderID = OrderID;	
		
		this.Positions = new ArrayList<Tuple2<Article,Integer>>();
		
		this.Picker = "";
		this.TransferPlace = "";
		
		this.Status = OPEN;
		
		this.pcs = new SimplePropertyChangeSupport(this);	
		
	}
	
	//-------- methods --------

	public synchronized String getOrderID() {
		return this.OrderID;
	}
	
	public synchronized List<Tuple2<Article,Integer>> getPositions() {
		return this.Positions;
	}
	public synchronized void addPosition(Article article, int quantity) {
		List<Tuple2<Article,Integer>> old = this.Positions;
		Tuple2<Article,Integer> tuple = new Tuple2<Article,Integer>(article,quantity);
		this.Positions.add(tuple);
		pcs.firePropertyChange("Positions", old, this.Positions);	
	}
	
	public synchronized String getPicker() {
		return this.Picker;
	}
	public synchronized void setPicker(String picker) {
		String old = this.Picker;
		this.Picker = picker;
		pcs.firePropertyChange("Picker", old, this.Picker);
	}
	
	public synchronized String getTransferPlace() {
		return this.TransferPlace;
	}
	public synchronized void setTransferPlace(String transferplace) {
		String old = this.TransferPlace;
		this.TransferPlace = transferplace;
		pcs.firePropertyChange("TransferPlace", old, this.TransferPlace);
	}
	
	public synchronized String getStatus() {
		return this.Status;
	}
	public synchronized void setStatus(String status) {
		String old = this.Status;
		this.Status = status;
		pcs.firePropertyChange("Status", old, status);
	}

	//-------- property methods --------

	public void addPropertyChangeListener(PropertyChangeListener listener) {
		pcs.addPropertyChangeListener(listener);
	}
	
	public void removePropertyChangeListener(PropertyChangeListener listener) {
		pcs.removePropertyChangeListener(listener);
	}
	
}

