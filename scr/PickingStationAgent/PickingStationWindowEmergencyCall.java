package PickingStationAgent;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JLabel;
import java.awt.Dimension;
import javax.swing.border.LineBorder;
import java.awt.Color;
import javax.swing.SwingConstants;
import java.awt.Font;

public class PickingStationWindowEmergencyCall {

	public JFrame frmEmergencyCall;	
	public JTextArea taStatus;		
	public JButton btnEnvironmentModified;
	public JButton btnRetryObjectDetection;
	public JButton btnReleaseRobot;	
	public JLabel lblPicture;
	public JLabel lblArticle;
	
	public PickingStationWindowEmergencyCall() {
		initialize();
	}

	private void initialize() {
		
		frmEmergencyCall = new JFrame();
		frmEmergencyCall.setResizable(false);
		frmEmergencyCall.setTitle("Emergency Call");
		frmEmergencyCall.setBounds(100, 100, 942, 715);
		frmEmergencyCall.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel panel_Status = new JPanel();
		panel_Status.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_Button = new JPanel();
		
		JPanel panel_Picture = new JPanel();
		panel_Picture.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel_Picture.setPreferredSize(new Dimension(200, 200));
		GroupLayout groupLayout = new GroupLayout(frmEmergencyCall.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(panel_Button, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(10)
							.addComponent(panel_Status, GroupLayout.PREFERRED_SIZE, 238, GroupLayout.PREFERRED_SIZE)))
					.addGap(18)
					.addComponent(panel_Picture, GroupLayout.PREFERRED_SIZE, 628, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(20, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(panel_Button, GroupLayout.PREFERRED_SIZE, 261, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(panel_Status, GroupLayout.DEFAULT_SIZE, 388, Short.MAX_VALUE))
						.addComponent(panel_Picture, Alignment.TRAILING, GroupLayout.PREFERRED_SIZE, 643, GroupLayout.PREFERRED_SIZE))
					.addGap(20))
		);
		
		JScrollPane spStatus = new JScrollPane();
		panel_Status.add(spStatus, BorderLayout.CENTER);
		
		taStatus = new JTextArea();
		taStatus.setEditable(false);
		spStatus.setViewportView(taStatus);
		taStatus.setRows(7);
		panel_Picture.setLayout(new BorderLayout(0, 0));
		
		lblPicture = new JLabel("");
		lblPicture.setHorizontalTextPosition(SwingConstants.CENTER);
		lblPicture.setHorizontalAlignment(SwingConstants.CENTER);
		lblPicture.setSize(new Dimension(200, 200));
		panel_Picture.add(lblPicture, BorderLayout.CENTER);
		
		lblArticle = new JLabel("");
		lblArticle.setHorizontalAlignment(SwingConstants.CENTER);
		lblArticle.setFont(new Font("Tahoma", Font.PLAIN, 16));
		panel_Picture.add(lblArticle, BorderLayout.NORTH);
		
		btnEnvironmentModified = new JButton("Environment modified!");

		btnEnvironmentModified.setEnabled(false);
		
		btnRetryObjectDetection = new JButton("Retry object detection");

		btnRetryObjectDetection.setEnabled(false);
		
		btnReleaseRobot = new JButton("Release robot!");

		btnReleaseRobot.setEnabled(false);
		
		GroupLayout gl_panel_Button = new GroupLayout(panel_Button);
		gl_panel_Button.setHorizontalGroup(
			gl_panel_Button.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, gl_panel_Button.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel_Button.createParallelGroup(Alignment.TRAILING)
						.addComponent(btnReleaseRobot, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 240, Short.MAX_VALUE)
						.addComponent(btnEnvironmentModified, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 240, Short.MAX_VALUE)
						.addComponent(btnRetryObjectDetection, GroupLayout.DEFAULT_SIZE, 240, Short.MAX_VALUE))
					.addContainerGap())
		);
		gl_panel_Button.setVerticalGroup(
			gl_panel_Button.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_Button.createSequentialGroup()
					.addContainerGap()
					.addComponent(btnEnvironmentModified, GroupLayout.PREFERRED_SIZE, 68, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(btnRetryObjectDetection, GroupLayout.PREFERRED_SIZE, 68, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(btnReleaseRobot, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(13, Short.MAX_VALUE))
		);
		panel_Button.setLayout(gl_panel_Button);
		frmEmergencyCall.getContentPane().setLayout(groupLayout);
		
	}
}
