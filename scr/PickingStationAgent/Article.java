package PickingStationAgent;

import java.io.Serializable;
import jadex.bridge.service.annotation.Reference;
import jadex.commons.SimplePropertyChangeSupport;
import jadex.commons.beans.PropertyChangeListener;

@Reference
public class Article implements Serializable {

	//-------- constants --------
	
	private static final long serialVersionUID = 1L;
	
	//-------- attributes --------
	
	protected int ID;
	protected String Name;	
	protected String Group;	
	
	protected int Length;
	protected int Width;
	protected int Heigth;
	protected int Mass;
	
	protected String Shelf;
	
	protected double POD;
	protected String CNN;
	
	protected String PathFolderImage;
	
	public SimplePropertyChangeSupport pcs;
	
	//-------- constructors --------
	
	public Article(int id, String name, String group, String shelf, double pod, String cnn, String path) {
		
		this.ID = id;
		this.Name = name;
		this.Group = group;
		
		this.Length = 0;
		this.Width = 0;
		this.Heigth = 0;
		this.Mass = 0;
		
		this.Shelf = shelf;
		
		this.POD = pod;
		this.CNN = cnn;
		
		this.PathFolderImage = path;
			
		this.pcs = new SimplePropertyChangeSupport(this);
		
	}
	
	//-------- methods --------
	
	public synchronized int getID() {
		return this.ID;
	}	
	
	public synchronized String getName() {
		return this.Name;
	}
	
	public synchronized String getGroup() {
		return this.Group;
	}
	
	public synchronized int getLength() {
		return this.Length;
	}
	
	public synchronized int getWidth() {
		return this.Width;
	}
	
	public synchronized int getHeigth() {
		return this.Heigth;
	}
	
	public synchronized int getMass() {
		return this.Mass;
	}
	
	public synchronized String getShelf() {
		return this.Shelf;
	}
		
	public synchronized double getPOD() {
		return this.POD;
	}
	public synchronized void setPOD(double pod) {
		double old = this.POD;
		this.POD = pod;
		pcs.firePropertyChange("POD", old, this.POD);
	}	
	
	public synchronized String getCNN() {
		return this.CNN;
	}
	public synchronized void setCNN(String cnn) {
		String old = this.CNN;
		this.CNN = cnn;
		pcs.firePropertyChange("CNN", old, this.CNN);
	}
	
	public synchronized String getPathFolderImage() {
		return this.PathFolderImage;
	}
	
	//-------- property methods --------

	public void addPropertyChangeListener(PropertyChangeListener listener) {
		pcs.addPropertyChangeListener(listener);
	}
	
	public void removePropertyChangeListener(PropertyChangeListener listener) {
		pcs.removePropertyChangeListener(listener);
	}
	
}

