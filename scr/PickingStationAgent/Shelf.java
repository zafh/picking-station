package PickingStationAgent;

public class Shelf {

	public Shelf() {}
	
	public String Name;
	
	public double X;
	public double Y;
	public double Width;
	public double Height;
	
    @Override
    public String toString() {
        return "Shelf [" + 
        			"Name=" + Name + ", " +
        			"X=" + String.valueOf(X) + ", " +
        			"Y=" + String.valueOf(Y) + ", " +
        			"Width=" + String.valueOf(Width) + ", " +
        			"Height=" + String.valueOf(Height) + "]";
    }
	
}
