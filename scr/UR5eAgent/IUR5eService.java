package UR5eAgent;

import jadex.bridge.service.annotation.Timeout;
import jadex.commons.future.IFuture;

public interface IUR5eService {

	@Timeout(Timeout.NONE)
	public IFuture<Boolean> PickObject(String grasp);

}