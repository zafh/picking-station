package UR5eAgent;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import jadex.bdiv3.features.IBDIAgentFeature;
import jadex.bridge.IInternalAccess;
import jadex.bridge.component.IExecutionFeature;
import jadex.commons.future.Future;
import jadex.commons.future.IFuture;
import jadex.micro.annotation.Agent;
import jadex.micro.annotation.AgentArgument;
import jadex.micro.annotation.AgentBody;
import jadex.micro.annotation.AgentCreated;
import jadex.micro.annotation.AgentFeature;
import jadex.micro.annotation.AgentKilled;
import jadex.micro.annotation.Argument;
import jadex.micro.annotation.Arguments;
import jadex.micro.annotation.ProvidedService;
import jadex.micro.annotation.ProvidedServices;

@Agent
@Arguments({
	@Argument(name="argHostRoboter", clazz=String.class),
	@Argument(name="argPortRoboter", clazz=Integer.class)
	})
@ProvidedServices({
	@ProvidedService(type=IUR5eService.class)
	})
public class UR5eAgentBDI implements IUR5eService{

	//-------- arguments --------
	
	@AgentArgument
	protected String argHostRoboter;
	
	@AgentArgument
	protected int argPortRoboter;
	
	//-------- parameters --------

	@Agent
	protected IInternalAccess agent;
	
	@AgentFeature
	protected IExecutionFeature execFeature;
	
	@AgentFeature
	protected IBDIAgentFeature bdiFeature;
	
	// socket server
	protected String host;
	protected int port;
	protected ServerSocket server;
	protected Socket client;
    protected OutputStream output;
    protected PrintWriter writer;
    protected InputStream input;
    protected BufferedReader reader;
	
	//-------- beliefs ---------
	
	//-------- creation --------
	
	@AgentCreated
	public void agentCreated() throws UnknownHostException, IOException, InterruptedException {
		
		host = argHostRoboter;
		port = argPortRoboter;

		// start socket server
		server = new ServerSocket(30000);

		// wait for client connection (UR5e)
		client = server.accept();
        input = client.getInputStream();
        reader = new BufferedReader(new InputStreamReader(input));
        output = client.getOutputStream();
        writer = new PrintWriter(output, true);
		
		// System.out.println("UR5e-Agent wurde gestartet.");
		
	}	
	
	//-------- body --------
	
	@AgentBody
	public void agentBody() throws IOException {}
	
	//-------- termination --------
	
	@AgentKilled
	public void agentKilled() throws IOException {}
	
	//-------- goals --------

	//-------- plans ----------
	
	//-------- methods --------
	
	public String StartPicking(String grasp) throws IOException {
        
		String[] data = grasp.split(",");		
		String poseGrasp = "(" +  data[1] + ","
							   +  data[2] + ","
							   +  data[3] + ","
							   +  2.772   + ","
							   + -2.235   + ","
							   + -2.346   + ")";
		
		/** Die Werte f�r die Rotation m�ssten eigentlich ausgelesen werden.
		 ** Diese hier sind aus der GUI abgelesen, wenn der TCP senkrecht auf das Objekt zeigt.
		 ** Im URCap ist das die Option "Last Waypoint Orientation".
		 **/
		
    	// send data
	    writer.println(poseGrasp);
	    writer.flush();
	    
    	// receive data
        char[] buffer = new char[1024];
        reader.read(buffer);
        String message = new String(buffer);
        // System.out.println(message);
		
        return message;
        
	}	
   	
	//-------- services --------
	
	@Override
	public IFuture<Boolean> PickObject(String grasp) {
		
		Future<Boolean> response = new Future<Boolean>();
		
		try {
			StartPicking(grasp);
			response.setResult(true);
		} catch (IOException e) {
			e.printStackTrace();
			response.setResult(false);
		}

		return response;
		
	}

}