package Platform;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import PickingStationAgent.Article;
import jadex.base.PlatformConfiguration;
import jadex.base.Starter;
import jadex.bridge.IExternalAccess;
import jadex.bridge.service.search.SServiceProvider;
import jadex.bridge.service.types.cms.CreationInfo;
import jadex.bridge.service.types.cms.IComponentManagementService;

public class AgentFactory {
	
	public static void main(String[] args) throws IOException {

    	// configure agent plattform
    	PlatformConfiguration config = PlatformConfiguration.getDefaultNoGui();
	    	config.setPlatformName("PickingStation");
	    	config.getRootConfig().setNetworkName("PickingStationNetworkName");
	    	config.getRootConfig().setNetworkPass("PickingStationNetworkPassword");
	    	config.getRootConfig().setTrustedLan(true);
	    	config.getRootConfig().setAwareness(true);
	    
	    // start agent plattform
    	IExternalAccess platform = Starter.createPlatform(config).get();
        IComponentManagementService cms = SServiceProvider.getService(platform, IComponentManagementService.class).get();
        
        // create prediction server agent
        HashMap<String, Object> PredictionServerAgentData = new HashMap<String, Object>();
        	PredictionServerAgentData.put("argPathDarknet", "C:\\PickingStation\\Darknet");
        	PredictionServerAgentData.put("argPathData", "CNNRaspberry\\obj.data");
        	PredictionServerAgentData.put("argPathConfig", "CNNRaspberry\\yolov4-tiny.cfg");
        	PredictionServerAgentData.put("argPathWeights", "CNNRaspberry\\yolov4-tiny_final.weights");  
	        PredictionServerAgentData.put("argThresholdObjectDetection", 85.0);    
        cms.createComponent("PredictionServer-THU", "PredictionServerAgent.PredictionServerAgentBDI.class", new CreationInfo(PredictionServerAgentData)).getFirstResult();	
	        
        // create roboception agent
        HashMap<String, Object> RoboceptionAgentData = new HashMap<String, Object>();
        	RoboceptionAgentData.put("argHostCamera","192.168.101.13");
        	RoboceptionAgentData.put("argPathStream", "C:\\PickingStation\\GenICam\\bin");
        	RoboceptionAgentData.put("argPathRecord", "C:\\PickingStation\\Aufnahmen");
        cms.createComponent("RoboceptionCamera-THU", "RoboceptionAgent.RoboceptionAgentBDI.class", new CreationInfo(RoboceptionAgentData)).getFirstResult();	        

        // create UR5e agent
        HashMap<String, Object> UR5eAgentData = new HashMap<String, Object>();
        UR5eAgentData.put("argHostRoboter","192.168.101.2");
        UR5eAgentData.put("argPortRoboter",30000);
        cms.createComponent("UR5e-THU", "UR5eAgent.UR5eAgentBDI.class", new CreationInfo(UR5eAgentData)).getFirstResult();	             
      
        // create picking station agent
        List<Article> Articles = new ArrayList<Article>();
	        Articles.add(new Article(1 , "Raspberry-Box"   , "", "", 99.9, "", ""));
        HashMap<String, Object> PickingStationAgentData = new HashMap<String, Object>();
	        PickingStationAgentData.put("argArticles", Articles);
	        PickingStationAgentData.put("argPathShelfReference", "C:\\PickingStation\\ShelfReference.json");
	        PickingStationAgentData.put("argMqttBroker", "192.168.101.42:1883");
	        PickingStationAgentData.put("argPathYoloMark", "C:\\PickingStation\\YoloMark");
        cms.createComponent("PickingStation-THU", "PickingStationAgent.PickingStationAgentBDI.class", new CreationInfo(PickingStationAgentData)).getFirstResult();
    		
    }
    
}