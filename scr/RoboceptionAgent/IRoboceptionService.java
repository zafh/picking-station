package RoboceptionAgent;

import java.io.IOException;
import java.util.Map;

import jadex.bridge.service.annotation.Timeout;
import jadex.commons.future.IFuture;

public interface IRoboceptionService {
	
	@Timeout(Timeout.NONE)
	public IFuture<String> ComputeGrasp(Map<String,Object> parameter);
  
	@Timeout(Timeout.NONE)
	public IFuture<String> RecordImage() throws IOException, InterruptedException;

}