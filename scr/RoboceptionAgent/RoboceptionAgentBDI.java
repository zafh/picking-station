package RoboceptionAgent;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.databind.JsonNode;
import com.roboception.rcapi.core.Service;
import PredictionServerAgent.CommandShell;
import jadex.bridge.IInternalAccess;
import jadex.commons.future.Future;
import jadex.commons.future.IFuture;
import jadex.micro.annotation.Agent;
import jadex.micro.annotation.AgentArgument;
import jadex.micro.annotation.AgentBody;
import jadex.micro.annotation.AgentCreated;
import jadex.micro.annotation.AgentKilled;
import jadex.micro.annotation.Argument;
import jadex.micro.annotation.Arguments;
import jadex.micro.annotation.ProvidedService;
import jadex.micro.annotation.ProvidedServices;

@Agent
@Arguments({
	@Argument(name="argHostCamera", clazz=String.class),
	@Argument(name="argPathStream", clazz=String.class),
	@Argument(name="argPathRecord", clazz=String.class)
	})
@ProvidedServices({
	@ProvidedService(type=IRoboceptionService.class)
	})
public class RoboceptionAgentBDI implements IRoboceptionService{

	//-------- arguments --------
	
	@AgentArgument
	protected String argHostCamera;
	
	@AgentArgument
	protected String argPathStream;
	
	@AgentArgument
	protected String argPathRecord;
	
	//-------- parameters --------

	@Agent
	protected IInternalAccess agent;
	
	protected String host;
	protected Service service;
	protected String stream;
	protected String aufnahmen;
	
	//-------- beliefs ---------
	
	//-------- creation --------
	
	@AgentCreated
	public void agentCreated() {
		
		host = argHostCamera;
		stream = argPathStream;
		aufnahmen = argPathRecord;
		
		// System.out.println("Roboception-Agent has been started.");

	}	
	
	//-------- body --------
	
	@AgentBody
	public void agentBody() {}
	
	//-------- termination --------
	
	@AgentKilled
	public void agentKilled() {}
	
	//-------- goals --------
	
	//-------- plans ----------

	//-------- methods --------

	public double round(double value, int decimalPoints) {
		double d = Math.pow(10, decimalPoints);
		return Math.round(value * d) / d;
	}
	
	//-------- services --------
	
	@Override
	public IFuture<String> RecordImage() throws IOException, InterruptedException {
		
		Future<String> response = new Future<String>();

		CommandShell cs = new CommandShell( "Buffers per second" );
		
		String cmdChangeDirectory = "cd " + stream;
		cs.processInput(cmdChangeDirectory);

		String cmdObjectDetection = "gc_stream.exe -f png 02940526 ComponentSelector=Intensity PixelFormat=YCbCr411_8 ComponentEnable=1 ComponentSelector=Disparity ComponentEnable=0 n=1";
		
		cs.processInput(cmdObjectDetection);

		cs.process.waitFor();
			
		String image = "";
		for (int i=0; i<cs.Output.size(); i++) {				
			if (cs.Output.get(i).contains("Image")) {
				// CMD = Image 'image_1517171855.849830656_Intensity_00_00.png' stored
				String[] s = cs.Output.get(i).split(" ");
				image = s[1].substring(1, s[1].length()-1);
			}				
		}
		
		cs.dispose();

		// delete text file
		String textFile = image.replace(".png", "_param.txt");
		Path textPath = Paths.get(stream + "\\" + textFile);
		Files.delete(textPath);
		
		// copy image
		Path oldPath = Paths.get( stream + "\\" + image );
		Path newPath = Paths.get( aufnahmen + "\\" + image );
		Files.move(oldPath, newPath);
		
		response.setResult(newPath.toString());
		return response;
		
	}

	@Override
	public IFuture<String> ComputeGrasp(Map<String,Object> parameter) {
		
		Future<String> response = new Future<String>();
		
		double quality = 0.0;
		double x = 0.0;
		double y = 0.0;
		double z = 0.0;
		double Rx = 0.0;
		double Ry = 0.0;
		double Rz = 0.0;
		
		service = Service.connectTo( host, "rc_itempick", "compute_grasps" );	
		JsonNode json = (JsonNode) service.call( parameter );

		List<JsonNode> grasps = json.findValues("grasps");
		for (JsonNode grasp : grasps) {		
			JsonNode pose = grasp.findValue("pose");
			if (pose == null) {
				response.setResult(""); // kein Greifpunkt ermittelt
				return response;
			}
			quality = round( grasp.findValue("quality").asDouble() * 100, 2);
			JsonNode position = pose.findValue("position");
			x = round( position.findValue("x").asDouble(), 6 );
			y = round( position.findValue("y").asDouble(), 6 );
			z = round( position.findValue("z").asDouble(), 6 );
			JsonNode orientation = pose.findValue("position");
			Rx = round( orientation.findValue("x").asDouble(), 6 );
			Ry = round( orientation.findValue("y").asDouble(), 6 );
			Rz = round( orientation.findValue("z").asDouble(), 6 );
		}

		response.setResult( String.valueOf(quality) + "," 
						  + String.valueOf(x) + "," +  String.valueOf(y) + "," + String.valueOf(z) + "," 
				          + String.valueOf(Rx) + "," + String.valueOf(Ry)+ "," + String.valueOf(Rz) );
		
		return response;
		
	}
	
}